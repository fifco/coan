//
//  Log.swift
//  Coan
//
//  Created by Filip Čonka on 03/02/2021.
//

import Foundation

struct LogRecord: Codable, Identifiable {
    private (set) var id = UUID()
    var date: Date
    var capsuleId: CapsuleTypeId
    
    init(date: Date, capsuleId: CapsuleTypeId) {
        self.date = date
        self.capsuleId = capsuleId
    }
}

class Log: ObservableObject {
    @Published var records = [LogRecord]() {
        didSet {
            let encoder = JSONEncoder()
            
            if let encoded = try?
                encoder.encode(records) {
                UserDefaults.standard.set(encoded, forKey: "log")
            }
        }
    }
    
    init() {
        if let records = UserDefaults.standard.data(forKey: "log") {
            let decoder = JSONDecoder()
            
            if let decoded = try?
                decoder.decode([LogRecord].self, from: records) {
                    self.records = decoded
                    return
                }
        }
        
        self.records = []
    }
}
