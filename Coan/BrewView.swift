//
//  BrewView.swift
//  Coan
//
//  Created by Filip Čonka on 01/02/2021.
//

import SwiftUI

struct BrewView: View {
    @EnvironmentObject var capsules: Capsules
    @EnvironmentObject var log: Log
    
    @State private var showingPickedCapsule = false
    
    @State var randomCapsuleIndex: Int
    
    var body: some View {
        NavigationView {
            Button(action: {
                randomCapsuleIndex = capsules.items.indices.randomElement()!
                self.showingPickedCapsule.toggle()
                sendStatistics()
                
                capsules.items[randomCapsuleIndex].brewedAmount += 1
                capsules.items[randomCapsuleIndex].onStockAmount -= 1
                
                let logRecord = LogRecord(date: Date(), capsuleId: capsules.items[randomCapsuleIndex].id)
                
                
                log.records.append(logRecord)
            }) {
                    Image(systemName: "wand.and.stars")
                    Text("Pick Random Capsule")
            }.padding().background(RoundedRectangle(cornerRadius: 15).opacity(0.2))
            .navigationBarTitle("Let's Brew!")
            .sheet(isPresented: $showingPickedCapsule, content: {
                DetailView(capsule: capsules.items[self.randomCapsuleIndex])
            })
        }
    }
    
    func sendStatistics() {
        guard let encoded = try? JSONEncoder().encode(capsules.items[randomCapsuleIndex]) else {
            print("Failed to encode randomCapsule")
            return
        }
        
        let url = URL(string: "https://coan.conka.pro")!
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = encoded
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard data != nil else {
                print("No data in response: \(error?.localizedDescription ?? "Unknown error").")
                return
            }
        }.resume()
    }
}

struct BrewView_Previews: PreviewProvider {
    static var previews: some View {
        BrewView(randomCapsuleIndex: 0)
    }
}
