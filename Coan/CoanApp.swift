//
//  CoanApp.swift
//  Coan
//
//  Created by Filip Čonka on 01/02/2021.
//

import SwiftUI

@main
struct CoanApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
