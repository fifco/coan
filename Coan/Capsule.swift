//
//  Capsule.swift
//  Coan
//
//  Created by Filip Čonka on 01/02/2021.
//

import Foundation

struct Capsule: Identifiable, Codable {
    let id: CapsuleTypeId
    var onStockAmount = 0
    var brewedAmount = 0
}


