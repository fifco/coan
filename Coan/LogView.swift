//
//  LogView.swift
//  Coan
//
//  Created by Filip Čonka on 03/02/2021.
//

import SwiftUI

struct LogView: View {
    @EnvironmentObject var log: Log
    
    static let dateFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM y HH:mm"
        return formatter
    }()
    
    var body: some View {
        NavigationView{
            List {
                ForEach(log.records.reversed()) { record in
                    VStack(alignment: .leading){
                        Text("\(record.date, formatter: Self.dateFormat)").font(.footnote).textCase(/*@START_MENU_TOKEN@*/.uppercase/*@END_MENU_TOKEN@*/)
                        HStack {
                            Image(record.capsuleId.rawValue).resizable().frame(width: 16, height: 16)
                            Text("\(capsuleList[record.capsuleId]!.name)").foregroundColor(.accentColor)
                        }
                    }
                }
            }.navigationBarTitle("Log")
        }.listStyle(InsetGroupedListStyle())
    }
}

