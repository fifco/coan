//
//  DetailView.swift
//  Coan
//
//  Created by Filip Čonka on 01/02/2021.
//

import SwiftUI

struct DetailView: View {
    
    @State var capsule: Capsule

    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Spacer()
                Image(capsule.id.rawValue).resizable().aspectRatio(contentMode: .fit).frame(width: 256).padding()
                Spacer()
            }
            List {
                Section {
                    VStack(alignment: .leading) {
                        Text("NAME").bold().font(.footnote)
                        Text("\(capsuleList[capsule.id]!.name)").foregroundColor(.accentColor)
                    }.padding(.top, 10).padding(.bottom, 10)
                    VStack(alignment: .leading) {
                        Text("STRENGTH").bold().font(.footnote)
                        Text("\(capsuleList[capsule.id]!.strength)").foregroundColor(.accentColor)
                    }.padding(.top, 10).padding(.bottom, 10)
                }
                Section(header: Text("Your Collection")) {
                    VStack(alignment: .leading) {
                        Text("Currently in collection").bold().font(.footnote).textCase(.uppercase)
                        Text("\(capsule.onStockAmount)").foregroundColor(.accentColor)
                    }.padding(.top, 10).padding(.bottom, 10)
                    VStack(alignment: .leading) {
                        Text("Brewed to date").bold().font(.footnote)
                        Text("\(capsule.brewedAmount)").foregroundColor(.accentColor)
                    }.padding(.top, 10).padding(.bottom, 10)
                }
                Section(header: Text("Update count")) {
                    Stepper(value: $capsule.onStockAmount, in: 1...500, step: 1) {
                        Text("Current: \(capsule.onStockAmount)")
                    }
                }
            }
        }.listStyle(InsetGroupedListStyle())
    }
}
