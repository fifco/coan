//
//  Capsules.swift
//  Coan
//
//  Created by Filip Čonka on 01/02/2021.
//

import Foundation

class Capsules: ObservableObject {
    @Published var items = [Capsule]() {
        didSet {
            let encoder = JSONEncoder()
            
            if let encoded = try?
                encoder.encode(items) {
                UserDefaults.standard.set(encoded, forKey: "capsules")
            }
        }
    }
    
    init() {
        if let items = UserDefaults.standard.data(forKey: "capsules") {
            let decoder = JSONDecoder()
            
            if let decoded = try?
                decoder.decode([Capsule].self, from: items) {
                    self.items = decoded
                    return
                }
        }
        
        self.items = []
    }
}
