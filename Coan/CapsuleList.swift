//
//  CapsuleList.swift
//  Coan
//
//  Created by Filip Čonka on 01/02/2021.
//

import Foundation

let capsuleIds = [
    CapsuleTypeId.colombia,
    CapsuleTypeId.ethiopia,
    CapsuleTypeId.india,
    CapsuleTypeId.indonesia,
    CapsuleTypeId.nicaragua,
    
    CapsuleTypeId.napoli,
    CapsuleTypeId.venezia,
    CapsuleTypeId.ristretto,
    CapsuleTypeId.arpeggio,
    CapsuleTypeId.kazaar,
    CapsuleTypeId.roma,
    CapsuleTypeId.livanto,
    CapsuleTypeId.ristrettodecaf,
    CapsuleTypeId.arpeggiodecaf,
    
    CapsuleTypeId.capriccio,
    CapsuleTypeId.cosi,
    CapsuleTypeId.volluto,
    CapsuleTypeId.vollutodecaf,
    
    CapsuleTypeId.vanilla,
    CapsuleTypeId.caramel,
    CapsuleTypeId.cocoa,
    CapsuleTypeId.chiaro,
    CapsuleTypeId.scuro,
    CapsuleTypeId.corto,
    
    CapsuleTypeId.envivo,
    CapsuleTypeId.fortissio,
    CapsuleTypeId.linizio,
    CapsuleTypeId.vivalto,
    CapsuleTypeId.vivaltodecaf,
]

let capsuleList = [
    CapsuleTypeId.colombia: CapsuleType(id: CapsuleTypeId.colombia, name: "Colombia", strength: 6),
    CapsuleTypeId.ethiopia: CapsuleType(id: CapsuleTypeId.ethiopia, name: "Ethiopia", strength: 4),
    CapsuleTypeId.india: CapsuleType(id: CapsuleTypeId.india, name: "India", strength: 11),
    CapsuleTypeId.indonesia: CapsuleType(id: CapsuleTypeId.indonesia, name: "Indonesia", strength: 8),
    CapsuleTypeId.nicaragua: CapsuleType(id: CapsuleTypeId.nicaragua, name: "Nicaragua", strength: 5),
    
    CapsuleTypeId.napoli: CapsuleType(id: CapsuleTypeId.napoli, name: "Ispirazione Napoli", strength: 13),
    CapsuleTypeId.venezia: CapsuleType(id: CapsuleTypeId.venezia, name: "Ispirazione Venezia", strength: 8),
    CapsuleTypeId.ristretto: CapsuleType(id: CapsuleTypeId.ristretto, name: "Ispirazione Ristretto Italiano", strength: 10),
    CapsuleTypeId.arpeggio: CapsuleType(id: CapsuleTypeId.arpeggio, name: "Ispirazione Firenze Arpeggio", strength: 9),
    CapsuleTypeId.kazaar: CapsuleType(id: CapsuleTypeId.kazaar, name: "Ispirazione Palermo Kazaar", strength: 12),
    CapsuleTypeId.roma: CapsuleType(id: CapsuleTypeId.roma, name: "Ispirazione Roma", strength: 8),
    CapsuleTypeId.livanto: CapsuleType(id: CapsuleTypeId.livanto, name: "Ispirazione Genova Livanto", strength: 6),
    CapsuleTypeId.ristrettodecaf: CapsuleType(id: CapsuleTypeId.ristrettodecaf, name: "Ispirazione Ristretto Italiano Decaffeinato", strength: 10),
    CapsuleTypeId.arpeggiodecaf: CapsuleType(id: CapsuleTypeId.arpeggiodecaf, name: "Ispirazione Firenze Arpeggio Decaffeinato", strength: 9),
    
    
    CapsuleTypeId.capriccio: CapsuleType(id: CapsuleTypeId.capriccio, name: "Capriccio", strength: 5),
    CapsuleTypeId.cosi: CapsuleType(id: CapsuleTypeId.cosi, name: "Cosi", strength: 4),
    CapsuleTypeId.volluto: CapsuleType(id: CapsuleTypeId.volluto, name: "Voluto", strength: 4),
    CapsuleTypeId.vollutodecaf: CapsuleType(id: CapsuleTypeId.vollutodecaf, name: "Voluto Decaffeinato", strength: 4),
    
    CapsuleTypeId.vanilla: CapsuleType(id: CapsuleTypeId.vanilla, name: "Vanilla Eclair", strength: 0),
    CapsuleTypeId.caramel: CapsuleType(id: CapsuleTypeId.caramel, name: "Caramel Crème Brulee", strength: 0),
    CapsuleTypeId.cocoa: CapsuleType(id: CapsuleTypeId.cocoa, name: "Cocoa Truffle", strength: 0),
    CapsuleTypeId.chiaro: CapsuleType(id: CapsuleTypeId.chiaro, name: "Chiaro", strength: 0),
    CapsuleTypeId.scuro: CapsuleType(id: CapsuleTypeId.scuro, name: "Scuro", strength: 0),
    CapsuleTypeId.corto: CapsuleType(id: CapsuleTypeId.corto, name: "Corto", strength: 0),
    
    CapsuleTypeId.envivo: CapsuleType(id: CapsuleTypeId.envivo, name: "Envivo Lungo", strength: 9),
    CapsuleTypeId.fortissio: CapsuleType(id: CapsuleTypeId.fortissio, name: "Fortissio Lungo", strength: 8),
    CapsuleTypeId.linizio: CapsuleType(id: CapsuleTypeId.linizio, name: "Linizio Lungo", strength: 4),
    CapsuleTypeId.vivalto: CapsuleType(id: CapsuleTypeId.vivalto, name: "Vivalto Lungo", strength: 4),
    CapsuleTypeId.vivaltodecaf: CapsuleType(id: CapsuleTypeId.vivaltodecaf, name: "Vivalto Lungo Decaffeinato", strength: 4),
]
