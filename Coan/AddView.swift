//
//  AddView.swift
//  Coan
//
//  Created by Filip Čonka on 01/02/2021.
//

import SwiftUI

struct AddView: View {
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var capsules: Capsules
    
    @State private var type = 0
    @State private var quantity = 0
    
    let quantities = [10, 20, 30, 40, 50]
    
    var body: some View {
        NavigationView {
            Form {
                Picker("Type", selection: $type) {
                    ForEach(0 ..< capsuleIds.count) {
                        Text("\(capsuleList[capsuleIds[$0]]!.name)")
                    }
                }
                
                Picker("Quantity", selection: $quantity) {
                    ForEach(0 ..< quantities.count) {
                        Text("\(self.quantities[$0])")
                    }
                }.pickerStyle(SegmentedPickerStyle())
            }
            .navigationBarTitle("Add capsules")
            .navigationBarItems(trailing: Button("Save") {
                if let capsuleIndex = self.capsules.items.firstIndex(where: { $0.id == capsuleList[capsuleIds[type]]!.id }) {
                    self.capsules.items[capsuleIndex].onStockAmount += quantities[quantity]
                } else {
                    let item = Capsule(id: capsuleList[capsuleIds[type]]!.id, onStockAmount: quantities[quantity], brewedAmount: 0)
                    self.capsules.items.append(item)
                }
                
                self.presentationMode.wrappedValue.dismiss()
            })
        }
    }
}

