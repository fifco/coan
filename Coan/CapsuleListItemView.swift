//
//  CapsuleListItemView.swift
//  Coan
//
//  Created by Filip Čonka on 02/02/2021.
//

import SwiftUI

struct CapsuleListItemView: View {
    var capsule: Capsule
    
    var body: some View {
        NavigationLink(destination: DetailView(capsule: capsule)) {
            HStack {
                Image(capsule.id.rawValue)
                    .resizable()
                    .frame(width: 32, height: 32)
                
                VStack(alignment: .leading) {
                    Text(capsuleList[capsule.id]!.name).font(.headline)
                    Text("Strength: \(capsuleList[capsule.id]!.strength)")
                }
                
                Spacer()
                
                Text("\(capsule.onStockAmount)x")
            }
        }
    }
}
