//
//  CapsuleTypes.swift
//  Coan
//
//  Created by Filip Čonka on 01/02/2021.
//

import Foundation

enum CapsuleTypeId: String, Codable {
    case colombia
    case ethiopia
    case india
    case indonesia
    case nicaragua
    case arpeggio
    case arpeggiodecaf
    case capriccio
    case caramel
    case chiaro
    case cocoa
    case corto
    case cosi
    case envivo
    case fortissio
    case kazaar
    case linizio
    case livanto
    case napoli
    case ristretto
    case ristrettodecaf
    case roma
    case scuro
    case vanilla
    case venezia
    case vivalto
    case vivaltodecaf
    case volluto
    case vollutodecaf
}

struct CapsuleType: Codable, Hashable {
    let id: CapsuleTypeId
    let name: String
    let strength: Int
}
