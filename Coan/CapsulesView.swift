//
//  CapsulesView.swift
//  Coan
//
//  Created by Filip Čonka on 01/02/2021.
//

import SwiftUI

struct CapsulesView: View {
    @EnvironmentObject var capsules: Capsules
    
    @State private var showingAddCapsules = false
         
    var body: some View {
        NavigationView {
            List {
                ForEach(capsules.items) {
                    capsule in
                    CapsuleListItemView(capsule: capsule)
                }.onDelete(perform: removeItems)
            }
            .navigationBarTitle("Your collection")
            .navigationBarItems(trailing: Button(action: {
                self.showingAddCapsules.toggle()
            }) {
                Image(systemName: "plus")
                Text("Add")
            })
            .sheet(isPresented: $showingAddCapsules, content: {
                AddView()
            })
        }.listStyle(InsetGroupedListStyle())
    }
    
    func removeItems(at offsets: IndexSet) {
        capsules.items.remove(atOffsets: offsets)
    }
}

struct CapsulesView_Previews: PreviewProvider {
    static var previews: some View {
        CapsulesView()
    }
}
