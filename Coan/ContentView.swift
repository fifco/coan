//
//  ContentView.swift
//  Coan
//
//  Created by Filip Čonka on 01/02/2021.
//

import SwiftUI

struct ContentView: View {
    
    var capsules = Capsules()
    
    var log = Log()
         
    var body: some View {
        TabView {
            CapsulesView().tabItem {
                Image(systemName: "square.grid.2x2")
                Text("Collection")
            }
            BrewView(
                randomCapsuleIndex: 0
            ).tabItem {
                Image(systemName: "drop.fill")
                Text("Brew")
            }
            LogView().tabItem {
                Image(systemName: "calendar.badge.clock")
                Text("Log")
            }
        }.environmentObject(capsules).environmentObject(log)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
